use v5.28;
use utf8;
use Object::Pad;
class   App::MPD::Feeder::Options;

use Log::Any qw($log);
use Time::Duration qw(duration_exact);
use Time::Duration::Parse qw(parse_duration);

has $log_level           :reader = 'warn';
has $target_queue_length :reader = 10;
has $mpd_host            :reader = undef;
has $mpd_port            :reader = undef;
has $db_path             :reader = 'mpd-feeder';
has $db_user             :reader = undef;
has $db_password         :reader = undef;
has $min_album_interval  :reader = parse_duration('5h');
has $min_song_interval   :reader = parse_duration('13d');
has $min_artist_interval :reader = parse_duration('1h 15m');
has $skip_db_update      :reader = 0;
has $initial_reconnect_delay :reader = parse_duration('3 seconds');
has $max_reconnect_delay :reader = parse_duration('2 minutes');

method parse_command_line {
    Getopt::Long::GetOptions(
        'log-level=s'               => \$log_level,
        'skip-db-update!'           => \$skip_db_update,
        'tql|target-queue-length=n' => sub {
            $target_queue_length = parse_integer(pop);
        },
        'mpd-host=s'           => \$mpd_host,
        'mpd-port=s'           => \$mpd_port,
        'db-path=s'            => \$db_path,
        'db-user=s'            => \$db_user,
        'min-album-interval=s' => sub {
            $min_album_interval = parse_duration(pop);
        },
        'min-song-interval=s' => sub {
            $min_song_interval = parse_duration(pop);
        },
        'min-artist-interval=s' => sub {
            $min_artist_interval = parse_duration(pop);
        },
    ) or exit 1;
}

sub handle_config_option( $ini, $section, $option, $target_ref,
    $converter = undef )
{
    return undef unless exists $ini->{$section}{$option};

    my $value = $ini->{$section}{$option};

    $value = $converter->($value) if $converter;

    $$target_ref = $value;

    $log->trace("Option $section.$option = $value");
}

method dump {
    say "[mpd-feeder]";
    say "log_level = $log_level";
    say "";
    say "[mpd]";
    say "host = " . ( $mpd_host // '' );
    say "port = " . ( $mpd_port // '' );
    say "initial-reconnect-delay = "
        . duration_exact($initial_reconnect_delay);
    say "max-reconnect-delay = " . duration_exact($max_reconnect_delay);
    say "";
    say "[queue]";
    say "target-length = $target_queue_length";
    say "min-song-interval = " . duration_exact($min_song_interval);
    say "min-album-interval = " . duration_exact($min_album_interval);
    say "min-artist-interval = " . duration_exact($min_artist_interval);
    say "";
    say "[db]";
    say "path = " .     ( $db_path     // '' );
    say "user = " .     ( $db_user     // '' );
    say "password = " . ( $db_password // '' );
}

sub parse_integer($input) {
    die "Invalid integer value '$input'" unless $input =~ /^\+?\d{1,18}$/;
    return $input + 0;
}

method parse_config_file($path) {
    $log->trace("Parsing configuration file $path");

    use Config::INI::Reader;
    my $ini = Config::INI::Reader->read_file($path);

    handle_config_option( $ini => mpd => host => \$mpd_host );
    handle_config_option( $ini => mpd => port => \$mpd_port );
    handle_config_option(
        $ini => mpd => 'initial-reconnect-delay' => \$initial_reconnect_delay,
        \&parse_duration
    );
    handle_config_option(
        $ini => mpd => 'max-reconnect-delay' => \$max_reconnect_delay,
        \&parse_duration
    );

    handle_config_option( $ini => 'mpd-feeder' => log_level => \$log_level );

    handle_config_option(
        $ini => queue => 'target-length' => \$target_queue_length,
        \&parse_integer
    );
    handle_config_option(
        $ini => queue => 'min-song-interval' => \$min_song_interval,
        \&parse_duration
    );
    handle_config_option(
        $ini => queue => 'min-album-interval' => \$min_album_interval,
        \&parse_duration
    );
    handle_config_option(
        $ini => queue => 'min-artist-interval' => \$min_artist_interval,
        \&parse_duration
    );

    handle_config_option( $ini => db => path     => \$db_path );
    handle_config_option( $ini => db => user     => \$db_user );
    handle_config_option( $ini => db => password => \$db_password );

    # FIXME: complain about unknown sections/parameters
}

1;
