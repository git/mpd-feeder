use v5.28;
use utf8;
use Object::Pad 0.57;
class App::MPD::Feeder::Command
    :isa(App::MPD::Feeder);

use Log::Any qw($log);

method run(@args) {
    my $cmd = shift @args;

    if ( $cmd eq 'dump-config' ) {
        die "dump-config command accepts no arguments\n" if @args;

        $self->opt->dump;

        return 0;
    }

    if ( $cmd eq 'add-unwanted-artist' ) {
        die "Missing command arguments\n" unless @args;
        $self->set_db_needs_update(0);
        for my $artist (@args) {
            if ( $self->db->add_unwanted_artist($artist) ) {
                $log->info(
                    "Artist '$artist' added to the unwanted list\n");
            }
            else {
                $log->warn(
                    "Artist '$artist' already in the unwanted list\n");
            }
        }

        return 0;
    }

    if ( $cmd eq 'del-unwanted-artist' ) {
        die "Missing command arguments\n" unless @args;
        $self->set_db_needs_update(0);
        for my $artist (@args) {
            if ( $self->db->del_unwanted_artist($artist) ) {
                $log->info(
                    "Artist '$artist' deleted from the unwanted list\n");
            }
            else {
                $log->warn(
                    "Artist '$artist' is not in the unwanted list\n");
            }
        }

        return 0;
    }

    if ( $cmd eq 'list-unwanted-artists' ) {
        die "This command has no arguments\n" if @args;
        $self->set_db_needs_update(0);
        my $count = $self->db->walk_unwanted_artists( sub { say @_ } );
        say "Total unwanted artists: $count";

        return 0;
    }

    if ( $cmd eq 'add-unwanted-album' ) {
        die
            "Syntax: mpd-feeder add-unwanted-album «album name» by «artist name»\n"
            unless @args == 3 and $args[1] =~ /^by$/i;
        $self->set_db_needs_update(0);
        my ( $album, $artist ) = @args[ 0, 2 ];
        if ( $self->db->add_unwanted_album( $album, $artist ) ) {
            $log->info(
                "Album «$album» by «$artist» added to the unwanted list\n"
            );
        }
        else {
            $log->warn(
                "Album «$album» by «$artist» already in the unwanted list\n"
            );
        }

        return 0;
    }

    if ( $cmd eq 'del-unwanted-album' ) {
        die
            "Syntax: mpd-feeder del-unwanted-album «album name» by «artist name»\n"
            unless @args == 3 and $args[1] =~ /^by$/i;
        $self->set_db_needs_update(0);
        my ( $album, $artist ) = @args[ 0, 2 ];
        if ( $self->db->del_unwanted_album( $album, $artist ) ) {
            $log->info(
                "Album «$album» by «$artist» deleted from the unwanted list\n"
            );
        }
        else {
            $log->warn(
                "Album «$album» by «$artist» is not in the unwanted list\n"
            );
        }

        return 0;
    }

    if ( $cmd eq 'list-unwanted-albums' ) {
        die "This command has no arguments\n" if @args;
        $self->set_db_needs_update(0);
        my $count = $self->db->walk_unwanted_albums(
            sub ( $album, $artist ) { say "«$album» by «$artist»" } );
        say "Total unwanted albums: $count";

        return 0;
    }

    if ( $cmd eq 'one-shot' ) {
        die "one-shot command accepts no arguments\n" if @args;

        $self->queue_songs( undef, sub {$self->mpd->loop->stop} );
        $self->mpd->loop->run;
        return 0;
    }
    elsif ( $cmd eq 'single' ) {
        die "single command accepts no arguments\n" if @args;

        $self->queue_songs( 1, sub {$self->mpd->loop->stop} );
        $self->mpd->loop->run;
        return 0;
    }
    else {
        die "Unknown command '$cmd'";
    }
}

1;
