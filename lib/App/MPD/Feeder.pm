use v5.28;
use utf8;
use Object::Pad;
class App::MPD::Feeder;

use App::MPD::Feeder::DB;
use App::MPD::Feeder::Options;
use DBD::Pg;
use DBI;
use Getopt::Long;
use IO::Async::Signal;
use IO::Async::Timer::Countdown;
use IO::Async::Timer::Periodic;
use Log::Any qw($log);
use Net::Async::MPD;
use Object::Pad;
use Syntax::Keyword::Try;
use Time::Duration qw(duration_exact);

use constant UNDER_SYSTEMD => eval { require Linux::Systemd::Daemon };

has $cfg_file :reader;
has $opt :reader;
has $db :reader;
has $db_needs_update :writer = 1;
has $mpd :reader;
has $mpd_connected = 0;
has $playlist_needs_filling = 1;
has $quit_requested = 0;
has $reload_requested = 0;
has $idler;
has $last_mpd_comm;
has $reconnect_delay = 5;

use constant DEFAULT_CONFIG_FILE => '/etc/mpd-feeder/mpd-feeder.conf';

ADJUST {
    Getopt::Long::Configure('pass_through');
    Getopt::Long::GetOptions( 'cfg|config=s' => \$cfg_file );
    Getopt::Long::Configure('no_pass_through');

    $cfg_file //= DEFAULT_CONFIG_FILE if -e DEFAULT_CONFIG_FILE;

    $self->configure;

    $db_needs_update = 0 if $opt->skip_db_update;
}

method configure {
    my $new_opt = App::MPD::Feeder::Options->new;

    $new_opt->parse_config_file($cfg_file) if $cfg_file;

    $new_opt->parse_command_line;

    Log::Any::Adapter->set( Stderr => log_level => $new_opt->log_level );

    $log->debug( "Systemd integration "
            . ( UNDER_SYSTEMD ? "available" : "not available" ) );

    $opt = $new_opt;

    $reconnect_delay = $opt->initial_reconnect_delay;

    $db = App::MPD::Feeder::DB->new( opt => $opt );
}

method status($text, $log_level = undef) {
    Linux::Systemd::Daemon::sd_notify( status => $text ) if UNDER_SYSTEMD;
    $log->$log_level($text) if $log_level;
}

method init_mpd {
    return if $mpd;

    my %conn = ( auto_connect => 0 );
    $conn{host} = $opt->mpd_host if $opt->mpd_host;
    $conn{port} = $opt->mpd_port if $opt->mpd_port;

    $mpd = Net::Async::MPD->new(%conn);

    $mpd->on(
        close => sub {
            $log->warn("Connection to MPD lost");
            $mpd->loop->stop('disconnected');
            $mpd_connected = 0;
            $idler->cancel if $idler;
            undef $mpd;
            $self->init_mpd;
        }
    );
    $mpd->on(
        playlist => sub {
            $playlist_needs_filling = 1;
        }
    );
    $mpd->on(
        database => sub {
            $db_needs_update = 1;
        }
    );

    my $loop = $mpd->loop;

    my $int_signal_handler = sub {
        state $signal_count = 0;
        $signal_count++;

        if ( $signal_count > 1 ) {
            $log->warn("Another signal received (#$signal_count)");
            $log->warn("Exiting abruptly");
            exit 2;
        }

        $log->debug("Signal received. Stopping loop");
        $quit_requested = 1;
        $loop->stop('quit');
        $self->break_idle;
    };

    for (qw(TERM INT)) {
        $loop->add(
            IO::Async::Signal->new(
                name       => $_,
                on_receipt => $int_signal_handler,
            )
        );
    }

    $loop->add(
        IO::Async::Signal->new(
            name       => 'HUP',
            on_receipt => sub {
                $log->debug("SIGHUP received. Scheduling reload");
                $reload_requested = 1;
                $loop->stop('reload');
                $self->break_idle;
            },
        )
    );

    $loop->add(
        IO::Async::Signal->new(
            name       => 'USR1',
            on_receipt => sub {
                $log->debug(
                    "SIGUSR1 received. Dumping configuration to STDERR");
                my $old = select \*STDERR;
                try {
                    $opt->dump;
                }
                finally {
                    select $old;
                }
            },
        )
    );
}

method connect_db {
    $db->connect($opt);
}

method update_db( $force = undef ) {
    if ( !$db_needs_update and !$force ) {
        $log->debug("Skipping DB update");
        return;
    }

    $self->status('Updating song database', 'info');

    my $rows = $mpd->send('listallinfo')->get;

    $log->trace('got all songs from MPD');

    $self->status('Updating local song database', 'debug');
    $db->start_update;
    try {
        my $song_count;

        foreach my $entry (@$rows) {
            next unless exists $entry->{file};

            $self->db->store_song( $entry->{file},
                $entry->{AlbumArtist} // $entry->{Artist},
                $entry->{Album} );

            $song_count++;
        }

        my ($total_songs, $total_artists, $total_albums,
            $new_songs,   $new_artists,   $new_albums
        ) = $self->db->finish_update;

        $log->notice(
            "Updated data about $song_count songs (including $new_songs new), "
                . "$total_artists artists (including $new_artists new) "

                . "and $total_albums albums (including $new_albums new)"
        );

        $db_needs_update = 0;
    }
    catch {
        my $err = $@;
        $self->db->cancel_update;
        die $err;
    }
}

method queue_songs( $num = undef ) {
    if ( !defined $num ) {
        return unless $playlist_needs_filling;

        $self->status("Requesting playlist", 'trace');
        my $present = $mpd->send('playlist')->get // [];
        $present = scalar(@$present);

        if ( $present < $opt->target_queue_length ) {
            $log->notice( "Playlist contains $present songs. Wanted: "
                    . $opt->target_queue_length );
            $self->queue_songs( $opt->target_queue_length - $present );
        }
        else {
            $log->info("Playlist contains $present songs");
            $playlist_needs_filling = 0;
        }

        return;
    }

    $self->status("Looking for suitable songs to queue", 'debug');
    my @list = $self->db->find_suitable_songs($num);

    die "Found no suitable songs" unless @list;

    if ( @list < $num ) {
        $log->warn(
            sprintf(
                'Found only %d suitable songs instead of %d',
                scalar(@list), $num
            )
        );
    }

    $log->debug("About to add $num songs to the playlist");

    my @paths;
    for my $song (@list) {
        my $path = $song->{song};
        $path =~ s/"/\\"/g;
        push @paths, $path;
    }

    $log->notice( "Adding " . join( ', ', map {"«$_»"} @paths ) );
    $self->status('Adding songs to the playlist');

    # MPD needs raw bytes
    utf8::encode($_) for @paths;
    my @commands;
    for (@paths) {
        push @commands, [ add => "\"$_\"" ];
    }
    my $f = $mpd->send( \@commands );
    $f->on_fail( sub { die @_ } );
    $f->on_done(
        sub {
            $self->db->note_song_qeued($_) for @list;
            $playlist_needs_filling = 0;
        }
    );
    $f->get;
}

method reexec {
    $self->status("disconnecting and re-starting", 'info');
    Linux::Systemd::Daemon::sd_reloading() if UNDER_SYSTEMD;
    $db->disconnect;
    undef $mpd;

    my @exec = ( $0, '--config', $self->cfg_file, '--skip-db-update' );
    if ( $log->is_trace ) {
        $log->trace( 'exec ' . join( ' ', map { /\s/ ? "'$_'" : $_ } @exec ) );
    }
    exec(@exec);
}

method break_idle {
    if ( $idler && !$idler->is_ready ) {
        if ($mpd_connected) {
            $log->trace("hand-sending 'noidle'");
            undef $idler;
            $mpd->{mpd_handle}->write("noidle\n");
        }
        else {
            $log->trace("not connected to MPD: skipping 'noidle'");
        }
    }
    else {
        $log->trace("no idler found");
    }
}

method sleep_before_reconnection {
    $self->status(
        "Waiting for "
            . duration_exact($reconnect_delay)
            . " before re-connecting",
        'debug'
    );

    $mpd->loop->add(
        IO::Async::Timer::Countdown->new(
            delay     => $reconnect_delay,
            on_expire => sub { $mpd->loop->stop },
        )->start
    );

    $reconnect_delay = $reconnect_delay * 1.5;
    $reconnect_delay = 120 if $reconnect_delay > 120;
    $mpd->loop->run;
}

method pulse {
    unless ($mpd_connected) {
        $self->status("Connecting to MPD", 'trace');
        my $f = $mpd->connect->await;

        if ( $f->is_done ) {
            $mpd_connected          = 1;
            $playlist_needs_filling = 1;
            $reconnect_delay        = $opt->initial_reconnect_delay;
            $mpd->loop->later( sub { $self->pulse } );
        }
        elsif ( $f->is_failed ) {
            $mpd->loop->stop('disconnected');
            $log->warn($f->failure);
            $self->sleep_before_reconnection;
        }
        else {
            die "connect Future neither done nor failed!?";
        }

        return;
    }

    if ($db_needs_update) {
        $self->update_db;
        $mpd->loop->later( sub { $self->pulse } );
        return;
    }

    if ($playlist_needs_filling) {
        $self->queue_songs;
        $mpd->loop->later( sub { $self->pulse } );
        return;
    }

    $self->status("Waiting for playlist/database changes", 'debug');
    $last_mpd_comm = time;
    $idler         = $mpd->send("idle database playlist");
    $idler->await;

    $log->trace('got out of idle');

    if ($idler) {
        if ( $idler->is_done ) {
            my $result = $idler->get;
            undef $idler;
            if ( ref $result and $result->{changed} ) {
                my $changed = $result->{changed};
                $changed = [$changed] unless ref $changed;

                $mpd->emit($_) for @$changed;
            }
        }
        elsif ( $idler->is_cancelled ) {
            $log->trace("idle was cancelled");
            undef $idler;
        }
        elsif ( $idler->is_failed ) {
            $log->warn("idle failed: ".$idler->failure);
            undef $idler;
        }
    }

    $mpd->loop->stop;
}

method run_loop {
    $self->connect_db;

    $self->init_mpd;

    my $loop = $mpd->loop;

    $loop->add(
        IO::Async::Timer::Periodic->new(
            interval => 60,
            on_tick  => sub {
                if (!$mpd_connected) {
                    $log->trace("Not connected to MPD. Skipping alive check.");
                    $loop->stop('disconnected');
                    return;
                }

                if ( time - $last_mpd_comm > 300 ) {

                    $log->trace(
                        "no active MPD communication for more that 5 minutes");
                    $self->status("checking connection", 'debug');
                    $self->break_idle;
                }
                else {
                    $log->trace(
                        "contacted MPD less than 5 minutes ago. skipping alive check"
                    );
                }
            },
        )->start
    );

    for ( ;; ) {
        if ( $quit_requested ) {
            $self->status("about to quit", 'debug');
            Linux::Systemd::Daemon::sd_stopping() if UNDER_SYSTEMD;
            undef $mpd;
            $db->disconnect;
            last;
        }
        elsif ( $reload_requested ) {
            $self->reexec;
            die "Not reached";
        }

        $log->trace('About to run the loop');

        $mpd->loop->later( sub { $self->pulse } );

        Linux::Systemd::Daemon::sd_ready() if UNDER_SYSTEMD;
        $mpd->loop->run;
    }
}

1;
